import { useAuthStore } from "../stores/auth";
export default async (to, from, next) => {
  const auth = useAuthStore();
  if (!auth.check) {
    next({ name: "login" });
  } else {
    next();
  }
};
