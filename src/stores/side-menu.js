import { defineStore } from "pinia";

export const useSideMenuStore = defineStore({
  id: "side-menu",
  state: () => ({
    menu: [
      {
        header: "Menu",
        hiddenOnCollapse: true,
      },
      {
        title: "Dashboard",
        icon: {
          element: "font-awesome-icon",
          class: "p-2",
          attributes: {
            icon: "home",
          },
        },
        href: { name: "Home" },
        permission: "Home",
      },
      {
        title: "Facturacion",
        icon: {
          element: "font-awesome-icon",
          class: "p-2",
          attributes: {
            icon: "file-invoice-dollar",
          },
        },
        permission: "menu_facturacion",
        child: [
          {
            title: "Cargar Datos Facturación",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "facturacion.cargar" },
            permission: "facturacion.cargar",
          },
          {
            title: "Validar Facturación",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "facturacion.validar" },
            permission: "facturacion.validar",
          },
        ],
      },
      {
        title: "Radicación",
        icon: {
          element: "font-awesome-icon",
          class: "p-2",
          attributes: {
            icon: "file-lines",
          },
        },
        permission: "menu_radicacion",
        child: [
          {
            title: "Auditar Soportes",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "radicacion.listado" },
            permission: "radicacion.listado",
          },
          {
            title: "Gestión Información",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "radicacion.gestion-informacion" },
            permission: "radicacion.gestion-informacion",
          },
        ],
      },
      {
        title: "Mensajería SMS",
        icon: {
          element: "font-awesome-icon",
          class: "p-2",
          attributes: {
            icon: "envelope",
          },
        },
        permission: "menu_mensajeria",
        child: [
          {
            title: "Reporte General",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "mensajeria.reporte" },
            permission: "mensajeria.reporte",
          },
          {
            title: "Plantillas",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "mensajeria.plantillas" },
            permission: "mensajeria.plantillas",
          },
          {
            title: "Histórico",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "mensajeria.historico" },
            permission: "mensajeria.historico",
          },
        ],
      },
      {
        title: "Ajustes",
        icon: {
          element: "font-awesome-icon",
          class: "p-2",
          attributes: {
            icon: "gear",
          },
        },
        permission: "menu_ajustes",
        child: [
          {
            title: "Proveedores",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "ajustes.proveedores" },
            permission: "ajustes.proveedores",
          },
          {
            title: "CUP / CUM",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "ajustes.servicios" },
            permission: "ajustes.servicios",
          },
          {
            title: "Pacientes",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "ajustes.pacientes" },
            permission: "ajustes.pacientes",
          },
          {
            title: "Aseguradoras",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            permission: "menu_aseguradoras",
            child: [
              {
                title: "Listado Aseguradoras",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "ajustes.aseguradoras" },
                permission: "ajustes.aseguradoras",
              },
              {
                title: "Soportes / Servicios",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "ajustes.aseguradoras.soportestipificacion" },
                permission: "ajustes.aseguradoras.soportestipificacion",
              },
              {
                title: "Tipificación",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "ajustes.aseguradoras.tipificacion" },
                permission: "ajustes.aseguradoras.tipificacion",
              },
              {
                title: "Otros Nombres Aseguradoras",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "ajustes.aseguradoras.otrosnombres" },
                permission: "ajustes.aseguradoras.otrosnombres",
              },
              {
                title: "Tarifas CUM / CUP",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "ajustes.aseguradoras.tarifas" },
                permission: "ajustes.aseguradoras.tarifas",
              },
              {
                title: "Reglas de Validación",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "ajustes.aseguradoras.reglasvalidacion" },
                permission: "ajustes.aseguradoras.reglasvalidacion",
              },
            ],
          },
          {
            title: "Parametrizaciones",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            permission: "menu_parametrizaciones",
            child: [
              {
                title: "Información cliente",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "parametrizaciones.cliente-informacion" },
                permission: "parametrizaciones.cliente-informacion",
              },
              {
                title: "Accesos API",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "parametrizaciones.accesos-api" },
                permission: "parametrizaciones.accesos-api",
              },
              {
                title: "Asignacion Proveedores",
                icon: {
                  element: "font-awesome-icon",
                  class: "p-2",
                  attributes: {
                    icon: "minus",
                  },
                },
                href: { name: "parametrizaciones.asignacion-proveedores" },
                permission: "parametrizaciones.asignacion-proveedores",
              },
            ],
          },
          {
            title: "Configuraciones",
            icon: {
              element: "font-awesome-icon",
              class: "p-2",
              attributes: {
                icon: "minus",
              },
            },
            href: { name: "ajustes.config" },
            permission: "ajustes.config",
          },
        ],
      },
    ],
    isCollapsed: false,
    isOnMobile: false,
  }),

  getters: {
    collapsed: (state) => state.isCollapsed,
    isMobile: (state) => state.isOnMobile,
  },

  actions: {
    setCollapsed(value) {
      this.isCollapsed = value;
    },
    setIsMobile(value) {
      this.isOnMobile = value;
    },
  },
});
