import axios from "../../../../../libs/axios.lib"
export default (params = {}) => {
    return new Promise(resolve => {
        const baseURL = import.meta.env.VITE_VUE_APP_MICROSERVICE_API;
        const url = `${baseURL}/settings/aseguradoras/tarifas/export-template-xls`
        const res = axios.getUri({ url, params });
        resolve(res)
    })
}
