import { defineStore } from "pinia";
import Cookies from "js-cookie";
import axios from "../libs/axios.lib";

export const useAuthStore = defineStore({
  id: "auth",
  state: () => ({
    _user: null,
    _token: Cookies.get("token"),
    _refreshToken: Cookies.get("refreshToken"),
    _refreshTask: 0,
  }),
  getters: {
    token: (state) => state._token,
    user: (state) => state._user,
    check: (state) => state._user !== null,
  },
  actions: {
    saveToken(token) {
      this._token = token;
      Cookies.set("token", token, { expires: null });
    },
    async fetchUser() {
      try {
        const { data } = await axios.get("/auth/me", {
          headers: { loading: "true" },
        });
        // let userData = data;
        // userData["hasPermisos"] = userData.hasPermisos.map((permiso) => {
        //   return {
        //     id: permiso.permisosId,
        //     nombre: permiso.myPermisos.nombre,
        //   };
        // });
        this._user = data;
      } catch (e) {
        this.token = null;
        Cookies.remove("token");
      }
    },
    async logout() {
      try {
        // await axios.post("/api/logout", { headers: { loading: "true" } });
        this._user = null;
        this._token = null;

        Cookies.remove("token");
        Cookies.remove("refreshToken");
      } catch (e) {
        console.error(e);
      }
    },
    // refreshTokens({ commit, state, dispatch }) {
    //   const { refreshToken } = state;
    //   axios
    //     .post(
    //       "/auth/refreshtoken",
    //       { refreshToken },
    //       { headers: { loading: "true" } }
    //     )
    //     .then(({ data }) => {
    //       const { accessToken: token, refreshToken } = data;
    //       commit("SAVE_TOKEN", { token, refreshToken });
    //       dispatch("autoRefresh");
    //     })
    //     .catch((err) => {
    //       console.error(err.message);
    //     });
    // },
    // autoRefresh({ state, commit, dispatch }) {
    //   const { token } = state;
    //   const { exp } = jwt_decode(token);
    //   const now = dayjs(new Date());
    //   const fechaExp = dayjs(new Date(exp * 1000));
    //   let timeUntilRefresh = fechaExp.diff(now, "s");
    //   timeUntilRefresh -= 1 * 60;
    //   if (timeUntilRefresh <= 0) return false;
    //
    //   const refreshTask = setTimeout(
    //     () => dispatch("refreshTokens"),
    //     timeUntilRefresh * 1000
    //   );
    //   commit("refreshTask", refreshTask);
    // },
  },
});
