import axios from "../../../../libs/axios.lib";
export default (id) => axios.get(`/settings/services/delete/${id}`);