import axios from "../../../../../libs/axios.lib"
export default (queryParams) => axios.get(`/settings/aseguradoras/otros-nombres`, { params: { ...queryParams } })
