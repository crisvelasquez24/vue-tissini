import axios from "../../../../../libs/axios.lib";
export default (queryParams) =>
  axios.get(`/users/email-password`, { params: { ...queryParams } });
