import { useAuthStore } from "../stores/auth";
export default async (to, from, next) => {
  const auth = useAuthStore();
  if (!auth.check && auth.token) {
    try {
      await auth.fetchUser();
    } catch (e) {
      next({ name: "login" });
      console.error(e);
    }
  }
  next();
};
