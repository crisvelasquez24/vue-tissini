import axios from "../../../../libs/axios.lib";
export default (id) => axios.get(`/proveedores/${id}`);
