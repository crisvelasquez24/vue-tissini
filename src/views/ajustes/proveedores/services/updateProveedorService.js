import axios from "../../../../libs/axios.lib";
export default (payload, id) => axios.put(`/proveedores/${id}`, payload);
