/* import the fontawesome core */
import { library } from "@fortawesome/fontawesome-svg-core";
/* import font awesome icon component */
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
/* import specific icons */
import {
  faLock,
  faEnvelope,
  faUser,
  faEraser,
  faBuilding,
  faLifeRing,
  faVideo,
  faTicketAlt,
  faExclamationCircle,
  faCloud,
  faDownload,
  faExclamationTriangle,
  faCheckCircle,
  faTags,
  faInfoCircle,
  faThumbsUp,
  faFilePdf,
  faPaperPlane,
  faAsterisk,
  faReply,
  faHandshake,
  faHome,
  faCheck,
  faCalendarDay,
  faCalendarCheck,
  faFileLines,
  faGear,
  faMinus,
  faFileInvoiceDollar,
} from "@fortawesome/free-solid-svg-icons";

/* add icons to the library */
library.add(
  faLock,
  faEnvelope,
  faUser,
  faEraser,
  faBuilding,
  faLifeRing,
  faVideo,
  faTicketAlt,
  faExclamationCircle,
  faCloud,
  faDownload,
  faExclamationTriangle,
  faCheckCircle,
  faTags,
  faInfoCircle,
  faThumbsUp,
  faFilePdf,
  faPaperPlane,
  faAsterisk,
  faReply,
  faHandshake,
  faHome,
  faCheck,
  faCalendarDay,
  faCalendarCheck,
  faFileLines,
  faGear,
  faMinus,
  faFileInvoiceDollar
);

const install = (app) => {
  app.component("font-awesome-icon", FontAwesomeIcon);
};

export { install as default };
