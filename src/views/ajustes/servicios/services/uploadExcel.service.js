import axios from "../../../../libs/axios.lib";
export default (payload) =>
  axios.post("/settings/services/upload", payload, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
