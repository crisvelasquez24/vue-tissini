import axios from "../../../../libs/axios.lib"
export default (queryParams) => axios.get(`/localizations`, { params: { ...queryParams }, headers: { loading: 'si' } })
