import axios from "../../../../libs/axios.lib";
export default () =>
    axios.get(`/settings/get-service-type`);
