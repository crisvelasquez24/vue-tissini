import axios from "../../../../libs/axios.lib";
export default (code) => axios.get(`/settings/services/validate/${code}`);
