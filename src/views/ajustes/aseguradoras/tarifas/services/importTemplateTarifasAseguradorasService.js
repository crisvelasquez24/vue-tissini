import axios from "../../../../../libs/axios.lib"
export default (payload, idAseguradora) => axios.post(`/settings/aseguradoras/tarifas/${idAseguradora}/importar-template-xls`, payload);
