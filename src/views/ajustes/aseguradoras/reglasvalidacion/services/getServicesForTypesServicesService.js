import axios from "../../../../../libs/axios.lib"
export default (queryParams, id) =>
    axios.get(`/settings/get-service-type/${id}`, { params: { ...queryParams } });
