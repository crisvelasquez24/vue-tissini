import axios from "../../../libs/axios.lib";
export default (queryParams) =>
  axios.get(`/v3/categories/sections`, { params: { ...queryParams } });
