import axios from "../../../../../libs/axios.lib"
export default (payload, id) => axios.put(`/settings/aseguradoras/gestion-informacion/servicios/${id}`, payload)
