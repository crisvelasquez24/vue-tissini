import axios from "../../../../libs/axios.lib"
export default (id) => axios.get(`/settings/patients/${id}`)
