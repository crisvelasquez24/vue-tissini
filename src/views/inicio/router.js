import auth from "../../middleware/auth.middleware";
// import securityAccess from "../../middleware/securityAcces"

export default [
  {
    path: "/",
    component: () => import("../../layouts/page/Main.vue"),
    // beforeEnter: [auth],
    children: [
      {
        path: "/inicio",
        name: "inicio",
        component: () => import("./Main.vue"),
      },
      {
        path: "/recuperar",
        name: "recuperar",
        component: () => import("./Recuperar.vue"),
      },
    ],
  },
];
