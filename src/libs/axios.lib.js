import axios from "axios";
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
// import store from '../stores'
import { useAuthStore } from "@/stores/auth";
import router from "../router";
// import i18n from '~/plugins/i18n'

axios.defaults.baseURL = import.meta.env.VITE_VUE_APP_MICROSERVICE_API;

// Request interceptor
axios.interceptors.request.use((request) => {
  const { loading } = request.headers;
  if (!loading) document.body.classList.add("loading-indicator");
  const auth = useAuthStore();
  if (auth.token) {
    request.headers.common.Authorization = `Bearer ${auth.token}`;
  }
  return request;
});

// Response interceptor
axios.interceptors.response.use(
  (response) => {
    document.body.classList.remove("loading-indicator");
    return response;
  },
  (error) => {
    document.body.classList.remove("loading-indicator");
    const auth = useAuthStore();
    const { status } = error.response;
    if (status === 401 && auth.check) {
      auth.logout();
      router.push({ name: "auth.login" });
      Swal.fire({
        icon: "warning",
        title: "Mensaje",
        text: "La sesión ha expirado",
      });
    } else if (status === 403) {
      auth.logout();
      router.push({ name: "auth.login" });
      Swal.fire({
        icon: "warning",
        title: "Mensaje",
        text: "La sesión ha expirado",
      });
    } else if (status === 429) {
      Swal.fire({
        title: "Mensaje de error",
        icon: "error",
        text: "Demasiados ventanas abiertas, intente más tarde",
      });
    } else if (status >= 400) {
      Swal.fire({
        title: "Mensaje de error",
        icon: "error",
        text: error.response.data.message,
      });
    } else if (status >= 500) {
      serverError(error.response);
    }

    return Promise.reject(error);
  }
);

let serverErrorModalShown = false;
async function serverError(response) {
  if (serverErrorModalShown) {
    return;
  }

  if ((response.headers["content-type"] || "").includes("text/html")) {
    const iframe = document.createElement("iframe");

    if (response.data instanceof Blob) {
      iframe.srcdoc = await response.data.text();
    } else {
      iframe.srcdoc = response.data;
    }

    Swal.fire({
      html: iframe.outerHTML,
      showConfirmButton: false,
      customClass: { container: "server-error-modal" },
      didDestroy: () => {
        serverErrorModalShown = false;
      },
      grow: "fullscreen",
      padding: 0,
    });

    serverErrorModalShown = true;
  }
}

export default axios;
