import { defineStore } from "pinia";

export const useInfoClienteStore = defineStore({
  id: "InfoCliente",
  state: () => ({
    infoCliente: {
      nit: "",
      razonSocial: "",
      direccion: "",
      telefono: "",
      celular: "",
      email: "",
    },
  }),
  getters: {
    doubleCount: (state) => state.counter * 2,
  },
  actions: {
    increment() {
      this.counter++;
    },
  },
});
