import axios from "../../../../libs/axios.lib";
export default (queryParams) => {
  return new Promise((resolve) => {
    const baseURL = import.meta.env.VITE_VUE_APP_MICROSERVICE_API;
    const url = `${baseURL}/settings/services/export-excel`;
    const res = axios.getUri({ url, params: { filters: queryParams } });
    resolve(res);
  });
};
