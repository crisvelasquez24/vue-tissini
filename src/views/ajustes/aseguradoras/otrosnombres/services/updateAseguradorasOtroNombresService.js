import axios from "../../../../../libs/axios.lib"
export default (payload, id) => axios.put(`/settings/aseguradoras/otros-nombres/${id}`, payload);
