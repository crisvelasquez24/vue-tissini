import notFound from './views/404.vue'
import notAutorize from './views/401.vue'
export default [
  {
    path: '/not-autorize',
    name: 'error.401',
    component: notAutorize
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'error.404',
    component: notFound
  }
]
