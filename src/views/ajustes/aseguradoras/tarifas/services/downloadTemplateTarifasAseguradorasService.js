import axios from "../../../../../libs/axios.lib"
export default (params = {}) => {
    return new Promise(resolve => {
        const baseURL = import.meta.env.VITE_VUE_APP_MICROSERVICE_API;
        const url = `${baseURL}/settings/aseguradoras/tarifas/download-template-xls`
        const res = axios.getUri({ url, params });
        resolve(res)
    })
}
