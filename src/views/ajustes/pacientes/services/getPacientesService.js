import axios from "../../../../libs/axios.lib"
export default (queryParams) => axios.get(`/settings/patients`, { params: { ...queryParams }, headers: { loading: 'si' } })
