import axios from "../../../../libs/axios.lib";
export default (id) => axios.delete(`/settings/patients/${id}`);
