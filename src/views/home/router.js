import auth from "../../middleware/auth.middleware";
import securityAccess from "../../middleware/securityAcces"

export default [
  {
    path: "/",
    component: () => import("../../layouts/page/Main.vue"),
    beforeEnter: [auth],
    children: [
      {
        path: "/",
        name: "Home",
        component: () => import("../home/Main.vue"),
        beforeEnter: [securityAccess],
      },
    ],
  },
];
