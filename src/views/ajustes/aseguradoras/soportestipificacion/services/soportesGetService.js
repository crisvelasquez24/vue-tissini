import axios from "../../../../../libs/axios.lib"
export default (queryParams) => axios.get(`/settings/aseguradoras/gestion-informacion/soportes`, { params: { ...queryParams } })
