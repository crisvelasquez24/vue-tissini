import axios from "../../../../../libs/axios.lib";
export default (queryParams, id = "") =>
  id === ""
    ? axios.get(`/users`, { params: { ...queryParams } })
    : axios.get(`/users/${id}`, { params: { ...queryParams } });
