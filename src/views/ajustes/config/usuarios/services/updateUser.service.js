import axios from "../../../../../libs/axios.lib";
export default (payload, id) => axios.put(`/users/${id}`, payload);
