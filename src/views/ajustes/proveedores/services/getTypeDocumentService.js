import axios from "../../../../libs/axios.lib"
export default (queryParams) => axios.get(`/type-documents`, { params: { ...queryParams }, headers: { loading: 'si' } })
