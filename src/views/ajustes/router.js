import auth from "../../middleware/auth.middleware";
import securityAccess from "../../middleware/securityAcces";
export default [
  {
    path: "/ajustes",
    beforeEnter: [auth],
    redirect: { name: "ajustes.proveedores" },
    component: () => import("../../layouts/page/Main.vue"),
    children: [
      {
        path: "/ajustes/proveedores",
        name: "ajustes.proveedores",
        component: () => import("./proveedores/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/proveedores/registrar",
        name: "ajustes.proveedores.form",
        component: () => import("./proveedores/form/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/proveedores/:id/editar",
        name: "ajustes.proveedores.editar",
        component: () => import("./proveedores/editar/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/proveedores/:id/informacion",
        name: "ajustes.proveedores.info",
        component: () => import("./proveedores/info/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/cup-cum",
        name: "ajustes.servicios",
        component: () => import("./servicios/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/pacientes",
        name: "ajustes.pacientes",
        component: () => import("./pacientes/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/config",
        name: "ajustes.config",
        redirect: { name: "ajustes.config.usuarios" },
        component: () => import("./config/Main.vue"),
        children: [
          {
            path: "/ajustes/config/usuarios",
            name: "ajustes.config.usuarios",
            component: () => import("./config/usuarios/Main.vue"),
            beforeEnter: [securityAccess],
          },
          {
            path: "/ajustes/config/usuarios/add",
            name: "ajustes.config.usuarios.add",
            component: () => import("./config/usuarios/add/Main.vue"),
            beforeEnter: [securityAccess],
          },
          {
            path: "/ajustes/config/usuarios/add/:id/editar",
            name: "ajustes.config.usuarios.editar",
            component: () => import("./config/usuarios/add/Main.vue"),
            beforeEnter: [securityAccess],
          },
          {
            path: "/ajustes/config/roles",
            name: "ajustes.config.roles",
            component: () => import("./config/roles/Main.vue"),
            beforeEnter: [securityAccess],
          },
          {
            path: "/ajustes/config/roles/:id/editar",
            name: "ajustes.config.roles.editar",
            component: () => import("./config/roles/add/Main.vue"),
            beforeEnter: [securityAccess],
          },
          {
            path: "/ajustes/config/roles/add",
            name: "ajustes.config.roles.add",
            component: () => import("./config/roles/add/Main.vue"),
            beforeEnter: [securityAccess],
          },
          {
            path: "/ajustes/config/permisos",
            name: "ajustes.config.permisos",
            component: () => import("./config/permisos/Main.vue"),
            beforeEnter: [securityAccess],
          },
          {
            path: "/ajustes/config/permisos/:id/editar",
            name: "ajustes.config.permisos.editar",
            component: () => import("./config/permisos/add/Main.vue"),
            beforeEnter: [securityAccess],
          },
          {
            path: "/ajustes/config/permisos/add",
            name: "ajustes.config.permisos.add",
            component: () => import("./config/permisos/add/Main.vue"),
            beforeEnter: [securityAccess],
          },
        ],
      },
      {
        path: "/ajustes/pacientes/registrar",
        name: "ajustes.pacientes.form",
        component: () => import("./pacientes/form/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/pacientes/:id/editar",
        name: "ajustes.pacientes.editar",
        component: () => import("./pacientes/editar/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/pacientes/:id/informacion",
        name: "ajustes.pacientes.info",
        component: () => import("./pacientes/info/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/cup-cum/addService",
        name: "ajustes.servicios.add",
        component: () => import("./servicios/addService/Main.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/cup-cum/addService/:id/edit",
        name: "ajustes.servicios.edit",
        component: () => import("./servicios/addService/Main.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras",
        name: "ajustes.aseguradoras",
        component: () => import("./aseguradoras/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/registrar",
        name: "ajustes.aseguradoras.form",
        component: () => import("./aseguradoras/form/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/:id/editar",
        name: "ajustes.aseguradoras.editar",
        component: () => import("./aseguradoras/editar/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/otros-nombres",
        name: "ajustes.aseguradoras.otrosnombres",
        component: () => import("./aseguradoras/otrosnombres/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/soportes-tipificacion",
        name: "ajustes.aseguradoras.soportestipificacion",
        component: () =>
          import("./aseguradoras/soportestipificacion/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/soportes-tipificacion/soportes",
        name: "ajustes.aseguradoras.soportestipificacion.soportes",
        component: () =>
          import("./aseguradoras/soportestipificacion/soportes/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/soportes-tipificacion/soportes/registrar",
        name: "ajustes.aseguradoras.soportestipificacion.soportes.registrar",
        component: () =>
          import("./aseguradoras/soportestipificacion/soportes/form/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/soportes-tipificacion/servicios",
        name: "ajustes.aseguradoras.soportestipificacion.servicios",
        component: () =>
          import("./aseguradoras/soportestipificacion/servicios/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/soportes-tipificacion/servicios/registrar",
        name: "ajustes.aseguradoras.soportestipificacion.servicios.registrar",
        component: () =>
          import(
            "./aseguradoras/soportestipificacion/servicios/form/index.vue"
          ),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/otros-nombres/registrar",
        name: "ajustes.aseguradoras.otrosnombres.registrar",
        component: () => import("./aseguradoras/otrosnombres/form/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/tipificacion",
        name: "ajustes.aseguradoras.tipificacion",
        component: () => import("./aseguradoras/tipificacion/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/:id/tipificacion",
        name: "ajustes.aseguradoras.tipificacion.listar",
        component: () => import("./aseguradoras/tipificacion/cards/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/tarifas",
        name: "ajustes.aseguradoras.tarifas",
        component: () => import("./aseguradoras/tarifas/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/:id/tarifas",
        name: "ajustes.aseguradoras.tarifas.listar",
        component: () => import("./aseguradoras/tarifas/table/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/reglas-validacion",
        name: "ajustes.aseguradoras.reglasvalidacion",
        component: () => import("./aseguradoras/reglasvalidacion/index.vue"),
        beforeEnter: [securityAccess],
      },
      {
        path: "/ajustes/aseguradoras/:id/reglas-validacion",
        name: "ajustes.aseguradoras.reglasvalidacion.listar",
        component: () =>
          import("./aseguradoras/reglasvalidacion/cards/index.vue"),
        beforeEnter: [securityAccess],
      },
    ],
  },
];
