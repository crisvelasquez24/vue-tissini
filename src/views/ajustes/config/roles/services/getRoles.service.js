import axios from "../../../../../libs/axios.lib";
export default (queryParams, id = "") =>
  id === ""
    ? axios.get(`/roles`, { params: { ...queryParams } })
    : axios.get(`/roles/${id}`, { params: { ...queryParams } });
