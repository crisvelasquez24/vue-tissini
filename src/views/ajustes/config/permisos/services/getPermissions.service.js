import axios from "../../../../../libs/axios.lib";
export default (queryParams, id = "") =>
  id === ""
    ? axios.get(`/permissions`, { params: { ...queryParams } })
    : axios.get(`/permissions/${id}`, { params: { ...queryParams } });
