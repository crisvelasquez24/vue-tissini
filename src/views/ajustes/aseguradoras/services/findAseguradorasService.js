import axios from "../../../../libs/axios.lib"
export default (id) => axios.get(`/settings/aseguradoras/${id}/ver`)
