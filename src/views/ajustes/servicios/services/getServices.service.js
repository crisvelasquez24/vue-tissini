import axios from "../../../../libs/axios.lib";
export default (queryParams,id='') =>
  axios.get(`/settings/services/${id}`, { params: { ...queryParams } });
