import axios from "../../../../../libs/axios.lib";
export default (payload, id) => axios.put(`/roles/${id}`, payload);
