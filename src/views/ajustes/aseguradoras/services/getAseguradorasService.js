import axios from "../../../../libs/axios.lib"
export default (queryParams) => axios.get(`/settings/aseguradoras`, { params: { ...queryParams }, headers: { loading: 'si' } })
