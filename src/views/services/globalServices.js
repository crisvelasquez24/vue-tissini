import axios from "../../libs/axios.lib";
export default class GoblalService {
  verPdfService(key) {
    return new Promise((resolve) => {
      const baseURL = import.meta.env.VITE_VUE_APP_MICROSERVICE_API;
      const url = `${baseURL}/documentos/pdf/ver`;
      const res = axios.getUri({ url, params: { key } });
      resolve(res);
    });
  }
  verImageService(key) {
    return new Promise((resolve) => {
      const baseURL = import.meta.env.VITE_VUE_APP_MICROSERVICE_API;
      const url = `${baseURL}/documentos/image/ver`;
      const res = axios.getUri({ url, params: { key } });
      resolve(res)
      /** @type {Blob} */
      // fetch(res)
      //   .then((response) => response.blob())
      //   .then((blob) => {
      //     const url = window.URL.createObjectURL(blob);
      //     resolve(url);
      //   });
    });
  }
}
