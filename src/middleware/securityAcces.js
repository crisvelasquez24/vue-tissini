import { useAuthStore } from "../stores/auth";
export default (to, from, next) => {
  const auth = useAuthStore();
  const user = auth.user
  if (!user.admin) {
    const permissions = user.permissions
    const nameRoute = to.name
    if (!nameRoute) return next()
    if (permissions.some(a => a === nameRoute)) {
      return next()
    }
    // const { access } = to.meta
    // if (!access) return next()
    // if (access.length) {
    //   for (const permission of access) {
    //     if (permissions.some(a => a === permission)) {
    //       return next()
    //     }
    //   }
    // }
    return next({ name: 'error.401' })
  }

  next()
}
