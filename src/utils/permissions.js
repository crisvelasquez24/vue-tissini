import { useAuthStore } from "@/stores/auth";

const can = (rolesArray = [], namePermission) => {
  if (rolesArray.length > 0 || namePermission) {
    // const auth = useAuthStore();
    // const user = auth.user;
    // // return ["ADMINISTRADOR", ...permission].some((permiso) => {
    // return [...rolesArray].some((permiso) => {
    //   return (
    //     user.permissions.permissionsRoles.name.toUpperCase() ===
    //     permiso.toUpperCase()
    //   );
    // });
  }
};

const install = (app) => {
  app.config.globalProperties.$can = can;
};

export { install as default, can };
