import axios from "../../../../../libs/axios.lib"
export default (queryParams) => axios.get(`/settings/aseguradoras/gestion-informacion/servicios`, { params: { ...queryParams } })
