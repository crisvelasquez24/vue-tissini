import axios from "../../../../../libs/axios.lib"
export default (id) => axios.delete(`/settings/aseguradoras/type-rules/${id}`);
